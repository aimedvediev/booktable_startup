import {Navigation} from 'react-native-navigation';
import {Provider} from 'react-redux';

import HomeScreen from '@screens/homePage';
import SearchScreen from '@screens/searchPage';
import BookingsScreen from '@screens/bookingsPage';
import ProfileScreen from '@screens/profilePage';
import DescriptionsScreen from '@screens/descriptionPage';
import startMainTabs from '@screens/mainTabs';
import configureStore from '@redux/store/configureStore';

import Icon from "react-native-vector-icons/Ionicons";

const store = configureStore();

const homeIcon = Icon.getImageSource("ios-home", 15);

Navigation.registerComponent(
    "bookTableMob.HomeScreen",
    () => HomeScreen,
    store,
    Provider
);
Navigation.registerComponent(
    "bookTableMob.SearchScreen",
    () => SearchScreen,
    store,
    Provider
);
Navigation.registerComponent(
    "bookTableMob.BookingsScreen",
    () => BookingsScreen
);
Navigation.registerComponent(
    "bookTableMob.ProfileScreen",
    () => ProfileScreen
);
Navigation.registerComponent(
    "bookTableMob.DescriptionScreen",
    () => DescriptionsScreen,
    store,
    Provider
);

async function prepareIcons() {
    const icons = await Promise.all([
        Icon.getImageSource("ios-home", 15),
        Icon.getImageSource("ios-search", 15),
        Icon.getImageSource("md-calendar", 15),
        Icon.getImageSource("ios-person", 15),
    ]);
    const [home, search, bookings, profile] = icons;
    return {home, search, bookings, profile};
}

async function startApp() {
    const icons = await prepareIcons();

    Navigation.startTabBasedApp({
        tabs: [
            {
                screen: "bookTableMob.HomeScreen",
                label: "Home Screen",
                icon: icons.home,

            },
            {
                screen: "bookTableMob.SearchScreen",
                label: "Search",
                title: "Search",
                icon: icons.search,
                navigatorStyle: {
                    drawUnderTabBar: true,
                }
            },
            {
                screen: "bookTableMob.BookingsScreen",
                label: "Bookings",
                title: "Bookings",
                icon: icons.bookings,
                navigatorStyle: {
                    navBarTranslucent: true,
                    drawUnderTabBar: true,
                }
            },
            {
                screen: "bookTableMob.ProfileScreen",
                label: "Profile",
                title: "Profile",
                icon: icons.profile
            },
        ], tabsStyle: { // optional, **iOS Only** add this if you want to style the tab bar beyond the defaults
            tabBarButtonColor: 'silver',
            tabBarSelectedButtonColor: '#ff0000'
        }, appStyle: {
            tabBarButtonColor: 'silver',
            tabBarSelectedButtonColor: '#ff0000',
            tabFontSize: 10,
            selectedTabFontSize: 12,
            navBarComponentAlignment: 'center'
        }
    });
}

startApp();

