import React from 'react';
import {Image, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import styles from './styles';

import Icon from "react-native-vector-icons/Ionicons";

const HomeListItem = (props) => (
    <TouchableOpacity onPress={props.onItemPressed}>
        <View elevation={5} style={styles.listItem}>
            <Image
                source={props.restaurantImage}
                style={{width: "100%", height: "60%"}}
            />
            <View style={styles.itemRow}>
                <Image
                    source={{uri: "https://cdn3.iconfinder.com/data/icons/vacation-4/32/vacation_15-512.png"}}
                    style={{width: "10%", height: "100%"}}
                />
                <Text style={{
                    flexGrow: 1,
                    fontSize: 12,
                }}>{props.restaurantCuisine}</Text>
            </View>
            <View style={styles.itemRow}>
                <Text style={{
                    flexGrow: 1,
                    fontSize: 15,
                    fontWeight: 'bold',
                }}>{props.restaurantName}</Text>
                <Icon name="ios-star" size={15} color="red"/>
                <Text style={{
                    flexGrow: 0,
                    marginLeft: 3
                }}>4.4 (53)</Text>

            </View>
            <View style={{flexDirection: 'row', padding: 5}}>
                <Text style={{
                    flexGrow: 1,
                    color: "#03BB85",
                    fontSize: 12,
                }}>{props.restaurantAddress}</Text>
                <Text style={{
                    flexGrow: 0,
                    fontSize: 12,
                }}>{props.destinationByFoot}</Text>
            </View>
            {/*TODO have not mark in api*/}
            <View style={styles.itemRow}>
                <TouchableOpacity style={styles.disactiveButton}>
                    <Text style={{color: 'white'}}>17:45</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.activeButton}>
                    <Text style={{color: 'white'}}>18:00</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.activeButton}>
                    <Text style={{color: 'white'}}>18:15</Text>
                </TouchableOpacity>
            </View>
        </View>
    </TouchableOpacity>
);
//todo Button active etc...

export default HomeListItem;