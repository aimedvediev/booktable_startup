import {StyleSheet} from 'react-native';

export default StyleSheet.create({

    listItem: {
        width: "90%",
        height: "75%",
        margin: 10,
        backgroundColor: "#FDFDFD",
        padding:5,
        shadowColor: "#000000",
        shadowOpacity: 0.8,
        shadowRadius: 2,
        borderRadius: 5,
        shadowOffset: {
            height: 1,
            width: 1
        }
    },
    itemRow: {
        flexDirection: "row",
        padding: 5
    },
    activeButton: {
        padding: 3,
        paddingRight: 13,
        paddingLeft: 13,
        borderRadius: 5,
        backgroundColor: 'red',
        marginRight: 8,
        marginLeft: 8
    },
    disactiveButton: {
        padding: 3,
        paddingRight: 13,
        paddingLeft: 13,
        borderRadius: 5,
        backgroundColor: 'silver',
        marginRight: 8,
        marginLeft: 8
    }
});
