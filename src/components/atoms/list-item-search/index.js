import React from 'react';
import {Image, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import styles from './styles';

const SearchListItem = (props) => (
    <TouchableOpacity onPress={props.onItemPressed}>
        <View style={styles.listItem}>
            <Image
                source={props.restaurantImage}
                style={{width: "30%", height: "100%"}}/>
            <View>
                <View style={{
                    flexDirection: 'row'
                }}>
                    <Text style={{
                        marginLeft: 3,
                        flexGrow: 0.4
                    }}>{props.restaurantName}</Text>
                    <Text style={{
                        flexGrow: 0.5
                    }}>4.4 (4 345)</Text>

                </View>
                <View style={{
                    marginLeft: 3,
                    marginBottom: 5
                }}>
                    <Text>{props.restaurantCuisine}</Text>
                </View>
                <View style={{
                    flexDirection: 'row',
                    marginBottom: 5
                }}>
                    <Text style={{
                        marginLeft: 3,
                    }}>{props.restaurantAddress}</Text>
                </View>
                <View style={{
                    flexDirection: "row",
                    padding: 5,
                    justifyContent: "space-around"
                }}>
                    <TouchableOpacity style={{
                        padding: 3,
                        paddingRight: 13,
                        paddingLeft: 13,
                        borderRadius: 2,
                        backgroundColor: 'silver',
                    }}>
                        <Text>17:45</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{
                        padding: 3,
                        paddingRight: 13,
                        paddingLeft: 13,
                        borderRadius: 2,
                        backgroundColor: 'red',
                        marginRight: 20,
                        marginLeft: 20
                    }}>
                        <Text>18:00</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{
                        padding: 3,
                        paddingRight: 13,
                        paddingLeft: 13,
                        borderRadius: 2,
                        backgroundColor: 'red',
                    }}>
                        <Text>18:15</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    </TouchableOpacity>
);

export default SearchListItem;