import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    listItem: {
        width: "95%",
        height: 100,
        margin: 10,
        backgroundColor: "white",
        flexDirection: "row",
        alignItems: "center"
    },

});
