import React from 'react';
import {ScrollView, FlatList} from 'react-native';
import styles from './styles';

import HomeListItem from '@atoms/list-item-main';

const HomeRestaurantList = props => {


    return (
        <FlatList
            style={styles.bottomView}
            horizontal={true}
            data={props.restaurants}
            renderItem={(info) => (
                <HomeListItem
                    restaurantType={info.item.name}
                    restaurantName={info.item.name}
                    restaurantImage={info.item.image}
                    restaurantAddress={info.item.address}
                    restaurantCuisine={info.item.cuisine_name}
                    destinationByFoot={Number.parseInt(info.item.distance*1000)+" m"}
                    onItemPressed={() => props.onItemSelected(info.item.key)}
                />
            )}
        />
    );
};

export default HomeRestaurantList;
