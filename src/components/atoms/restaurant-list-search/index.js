import React from 'react';
import {FlatList} from 'react-native';
import styles from './styles';

import SearchListItem from '@atoms/list-item-search';

const SearchRestaurantList = props => {

    return (
        <FlatList
            style={styles.bottomView}
            data={props.restaurants}
            renderItem={(info) => (
                <SearchListItem
                    restaurantType={info.item.name}
                    restaurantName={info.item.name}
                    restaurantAddress={info.item.address}
                    restaurantImage={info.item.image}
                    restaurantCuisine={info.item.cuisine_name}
                    //onItemPressed={() => props.onItemSelected(info.item.key)}
                />
            )}
        />
    );
};

export default SearchRestaurantList;
