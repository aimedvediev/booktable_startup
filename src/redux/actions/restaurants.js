import {SET_RESTAURANTS} from './actionTypes';

export const getRestaurants = (latitude, longitude) => {
    return dispatch => {
        fetch(`https://dev.booktable.co/api/get-restaurants?dateselect=17-05-2019&timeselect=07:30 PM&persons=2&location=Warsaw, Poland&city=Warszawa&state=Masovian Voivodeship&country=Poland&lat=${latitude}&lon=${longitude}`, {
            method: "GET"
        })
            .catch(err => {
                alert("Something wrong");
                console.log(err);
            })
            .then(res => res.json())
            .then(parsedRes => {
                const restaurants = [];
                for (let key in parsedRes.restaurants.data) {
                    restaurants.push({
                        ...parsedRes.restaurants.data[key],
                        image: {
                            uri: "https://dev.booktable.co/" + parsedRes.restaurants.data[key].abs_path
                        },
                        key: key
                    });
                }
                dispatch(setRestaurants(restaurants));
            });
    };
};

export const setRestaurants = restaurants => {
    return {
        type: SET_RESTAURANTS,
        restaurants: restaurants
    }
};