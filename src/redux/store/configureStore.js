import {createStore, combineReducers, compose, applyMiddleware} from 'redux'
import thunk from 'redux-thunk';

import restaurantReducer from '../reducers/restaurants';

const rootReducer = combineReducers({
    restaurants: restaurantReducer,
});

let composeEnhancers = compose();

const configureStore = () => {
    return createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));
};

export default configureStore;