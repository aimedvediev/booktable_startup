import React, {Component} from 'react';
import {View, Text} from 'react-native';
import {connect} from "react-redux";
import {getRestaurants} from '@redux/actions/index';

class DescriptionsScreen extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.onLoadPlaces();
    }

    render() {
        return (
            <View>
                <Text>On DescriptionsScreen:</Text>
                <Text>{this.props.selectedRes.name}</Text>
            </View>
        );
    }
}

const mapStateToProps = state => {
    return {
        restaurants: state.restaurants.restaurants
    };
};
const mapDispatchToProps = dispatch => {
    return {
        onLoadPlaces: () => dispatch(getRestaurants())
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(DescriptionsScreen);