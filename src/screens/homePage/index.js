import {
    Modal,
    ImageBackground,
    PermissionsAndroid,
    Text,
    TouchableOpacity,
    View,
    ScrollView,
    Image
} from "react-native";
import {Navigation} from 'react-native-navigation';
import {connect} from 'react-redux';
import React, {Component} from 'react';
import styles from './styles'

import HomeListItem from '@atoms/list-item-main';
import HomeRestaurantList from '@atoms/restaurant-list-main';
import {getRestaurants} from '@redux/actions/index';
import Icon from "react-native-vector-icons/Ionicons";
import {setRestaurants} from "../../redux/actions/restaurants";

class HomeScreen extends Component {
    static navigatorStyle = {
        navBarHidden: true,
        drawUnderTabBar: false
    };

    constructor(props) {
        super(props);
        this.state = {
            isModalVisible: false,
            latitude: 0,
            longitude: 0,
            howManyPeople: 2
        };
    }

    async componentDidMount() {
        await request_location_runtime_permission();
        navigator.geolocation.watchPosition(
            position => {
                this.setState({
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                });
                this.props.onLoadPlaces(this.state.latitude, this.state.longitude);
            },
            error => console.error(error),
            {enableHighAccuracy: true, timeout: 1000, maximumAge: 1000}
        );
    }

    setModalVisible(visible) {
        this.setState({isModalVisible: visible});
    }

    addPerson = () => {
        if (this.state.howManyPeople < 10) {
            this.setState({
                howManyPeople: this.state.howManyPeople + 1
            });
        }
    };

    removePerson = () => {
        if (this.state.howManyPeople > 0) {
            this.setState({
                howManyPeople: this.state.howManyPeople - 1
            });
        }
    };

    goToSeachPageEvent = () => {
        this.props.navigator.push({
            screen: "bookTableMob.SearchScreen",
            navigatorStyle: {
                tabBarHidden: true,
                navBarTranslucent: true,
                drawUnderNavBar: true,
                navBarTransparent: true,
            }
        });
    };
    goToDescriptionPageEvent = key => {
        const selRes = this.props.restaurants.find(restaurants => {
            return restaurants.key === key;
        });
        this.props.navigator.push({
            screen: "bookTableMob.DescriptionScreen",
            title: selRes.key,
            passProps: {
                selectedRes: selRes
            },
            navigatorStyle: {
                tabBarHidden: true,
            }
        });
    };

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.topView}>
                    <ImageBackground
                        source={{uri: "https://i.pinimg.com/originals/17/83/7b/17837b86f9487939449fdfd663cb0e54.jpg"}}
                        style={{width: '100%', height: '100%', flexDirection: 'column'}}>
                        <View style={styles.buttonPicker}>
                            <TouchableOpacity style={styles.button} onPress={this.goToSeachPageEvent}>
                                <Text style={{textAlign: 'center', fontSize: 17, marginTop: 5}}><Icon name="ios-pin"
                                                                                                      size={18}/> Warszawa</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.button} onPress={() => {
                                this.setModalVisible(!this.state.isModalVisible)
                            }}>
                                <Text style={{textAlign: 'center', fontSize: 17, marginTop: 5}}>
                                    <Icon name="ios-person" size={18}/> {this.state.howManyPeople} <Icon name="ios-time"
                                                                                                         size={18}/> Now</Text>
                            </TouchableOpacity>
                        </View>
                    </ImageBackground>
                </View>
                <Text style={{
                    fontStyle: "normal",
                    marginTop: 20,
                    marginLeft: 10,
                    marginBottom: 15,
                    fontSize: 20,
                    fontWeight: 'bold',
                }}>Make table reservation</Text>

                <HomeRestaurantList
                    restaurants={this.props.restaurants}
                    onItemSelected={this.goToDescriptionPageEvent}
                />
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.isModalVisible}
                    onRequestClose={() => {
                        Alert.alert('Modal has been closed.');
                    }}>
                    <View style={{
                        flex: 1,
                        alignItems: 'flex-end',
                        justifyContent: "flex-end",
                        backgroundColor: '#00000080',
                    }}>
                        <View style={{
                            height: "40%",
                            width: "100%",
                            backgroundColor: '#fff',
                        }}>
                            <View style={{flex: 1, flexDirection: "row"}}>
                                <TouchableOpacity style={{
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    width: 35,
                                    height: 35,
                                    padding: 3,
                                    borderRadius: 50,
                                    borderColor: "red",
                                    borderWidth: 1,
                                    margin: 5,
                                    shadowColor: "#000000",
                                    shadowOpacity: 0.8,
                                    shadowRadius: 2,
                                    shadowOffset: {
                                        height: 1,
                                        width: 1
                                    }
                                }}
                                                  onPress={this.addPerson}>
                                    <Text style={{fontSize: 20}}>+</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={{
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    width: 35,
                                    height: 35,
                                    padding: 3,
                                    borderRadius: 50,
                                    borderColor: "red",
                                    borderWidth: 1,
                                    margin: 5,
                                    shadowColor: "#000000",
                                    shadowOpacity: 0.8,
                                    shadowRadius: 2,
                                    shadowOffset: {
                                        height: 1,
                                        width: 1
                                    }
                                }}
                                                  onPress={this.removePerson}>
                                    <Text>-</Text>
                                </TouchableOpacity>
                                <Text style={{fontSize: 25}}> {this.state.howManyPeople} people</Text>
                            </View>
                            <TouchableOpacity
                                style={{
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    marginBottom: "20%",
                                    borderColor: "red",
                                    borderWidth: 1,
                                    padding: 5,
                                    margin: 5,
                                    shadowColor: "#000000",
                                    shadowOpacity: 0.8,
                                    shadowRadius: 2,
                                    shadowOffset: {
                                        height: 1,
                                        width: 1
                                    }
                                }}
                                onPress={() => {
                                    this.setModalVisible(!this.state.isModalVisible);
                                }}>
                                <Text>Done</Text>
                            </TouchableOpacity>
                        </View>

                    </View>
                </Modal>
            </View>
        );
    }
}

const mapStateToProps = state => {
    return {
        restaurants: state.restaurants.restaurants
    };
};
const mapDispatchToProps = dispatch => {
    return {
        onLoadPlaces: (latitude, longitude) => dispatch(getRestaurants(latitude, longitude))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);

export async function request_location_runtime_permission() {

    try {
        const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
            {
                'title': 'ReactNativeCode Location Permission',
                'message': 'ReactNativeCode App needs access to your location '
            }
        )
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {

            Alert.alert("Location Permission Granted.");
        } else {

            Alert.alert("Location Permission Not Granted");

        }
    } catch (err) {
        console.warn(err)
    }
}