import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 2,
        backgroundColor: 'white',
    },
    topView: {
        flex: 0.8,
    },
    bottomView: {
        flex: 1.2,
    },
    buttonPicker: {
        position: 'absolute',
        bottom: 10,
        alignSelf: "center",
        flexDirection: "row",

    },
    button: {
        backgroundColor: "white",
        borderRadius: 5,
        height: 30,
        width: 150,
        marginRight: 10,
        marginBottom: 20,
    },

});
