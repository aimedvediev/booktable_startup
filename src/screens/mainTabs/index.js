import {Navigation} from 'react-native-navigation';
import Icon from 'react-native-vector-icons/Ionicons';

const startMainTabs = () => {
    Promise.all([
        Icon.getImageSource("ios-home", 15),
        Icon.getImageSource("ios-search", 15),
        Icon.getImageSource("md-calendar", 15),
        Icon.getImageSource("ios-person", 15),
    ]).then(sources => {
        Navigation.startTabBasedApp({
            tabs: [
                {
                    screen: "bookTableMob.HomeScreen",
                    label: "Home Screen",
                    icon: sources[0],

                },
                {
                    screen: "bookTableMob.SearchScreen",
                    label: "Search",
                    title: "Search",
                    icon: sources[1]
                },
                {
                    screen: "bookTableMob.BookingsScreen",
                    label: "Bookings",
                    title: "Bookings",
                    icon: sources[2]
                },
                {
                    screen: "bookTableMob.ProfileScreen",
                    label: "Profile",
                    title: "Profile",
                    icon: sources[3]
                },
            ]
        });
    });
};

export default startMainTabs;