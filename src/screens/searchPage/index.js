import React, {Component} from 'react';
import {View, Text, ImageBackground, TouchableOpacity, ScrollView, Image, TextInput, Animated} from 'react-native';
import styles from "./styles";

import SearchListItem from '@atoms/list-item-search';
import SearchRestaurantList from '@atoms/restaurant-list-search'
import {getRestaurants} from '@redux/actions/index';
import {connect} from "react-redux";
import Icon from "react-native-vector-icons/Ionicons";

class SearchScreen extends Component {
    static navigatorStyle = {
        navBarTranslucent: true,
        drawUnderNavBar: true,
        navBarTransparent: true,
    };
    state = {
        placesLoaded: false,
        removeAnim: new Animated.Value(1),
        placesAnim: new Animated.Value(0)
    };

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.onLoadPlaces();
    }

    placesLoadedHandler = () => {
        Animated.timing(this.state.placesAnim, {
            toValue: 1,
            duration: 10,
            useNativeDriver: true
        }).start();
    };
    placesSearchHandler = () => {
        Animated.timing(this.state.removeAnim, {
            toValue: 0,
            duration: 10,
            useNativeDriver: true
        }).start(() => {
            this.setState({
                placesLoaded: true
            });
            this.placesLoadedHandler();
        });
    };

    render() {
        let content = (
            <Animated.View
                style={{
                    opacity: this.state.removeAnim,
                    transform: [
                        {
                            scale: this.state.removeAnim.interpolate({
                                inputRange: [0, 1],
                                outputRange: [2, 1]
                            })
                        }
                    ]
                }}>
                <TouchableOpacity onPress={this.placesSearchHandler}>
                    <View style={styles.searchButton}>
                        <Text style={styles.searchButtonText}>Find Places</Text>
                    </View>
                </TouchableOpacity>
            </Animated.View>
        );
        if (this.state.placesLoaded) {
            content = (
                <Animated.View style={{
                    opacity: this.state.placesAnim
                }}>
                    <SearchRestaurantList
                        restaurants={this.props.restaurants}
                    />
                </Animated.View>
            );
        }
        return (
            <View style={styles.container}>
                <View style={styles.topView}>
                    <ImageBackground
                        source={{uri: "https://i.pinimg.com/originals/17/83/7b/17837b86f9487939449fdfd663cb0e54.jpg"}}
                        style={{width: '100%', height: '100%', flex:3}}>
                        <View style={{
                            flexDirection: "row",
                            flex:1,
                            marginTop: 100,
                            alignItems: "center",
                        }}>
                            <TouchableOpacity style={styles.button} onPress={this.goToSeachPageEvent}>
                                <Text style={{textAlign: 'center', fontSize: 17}}><Icon name="ios-person"
                                                                                                      size={18}/> 2
                                    people</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.button} onPress={this.goToSeachPageEvent}>
                                <Text style={{textAlign: 'center', fontSize: 17}}><Icon
                                    name="ios-calendar" size={18}/> 01-05-2019</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.button} onPress={this.goToSeachPageEvent}>
                                <Text style={{textAlign: 'center', fontSize: 17}}><Icon name="ios-time" size={18}/> 18:30</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{
                            flexDirection: "row",
                            alignItems: "center",
                            flex:1
                        }}>
                            <TextInput placeholder="Type Address..." style={styles.inputAddress}/>
                        </View>
                        <View style={{flexDirection: 'row', flex:1}}>
                            <TouchableOpacity style={styles.button} onPress={this.goToSeachPageEvent}>
                                <Text style={{textAlign: 'center'}}>Cusine</Text>
                            </TouchableOpacity>
                            <TextInput placeholder="Name of restaurant" style={styles.inputName}/>
                        </View>
                    </ImageBackground>
                </View>
                <View style={this.state.placesLoaded ? null : styles.buttonContainer}>
                    {content}
                </View>
            </View>
        );
    }
}

const mapStateToProps = state => {
    return {
        restaurants: state.restaurants.restaurants
    };
};
const mapDispatchToProps = dispatch => {
    return {
        onLoadPlaces: () => dispatch(getRestaurants())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchScreen);