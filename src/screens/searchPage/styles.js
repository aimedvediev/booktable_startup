import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
    },
    topView: {
        width: "100%",
        height: "30%",
    },
    bottomView: {
        flex: 3
    },
    buttonPicker: {
        position: 'absolute',
        bottom: 0,
        right: 0,
    },
    button: {
        backgroundColor: "white",
        borderRadius: 5,
        borderWidth: 1,
        height: 30,
        margin: "2%",
        paddingRight: "3%",
        paddingLeft: "3%",
    },
    listItem: {
        width: "90%",
        height: "90%",
        margin: 10,
        backgroundColor: "#eee",
        flexDirection: "row",
        alignItems: "center",
        borderWidth: 1,
        borderRadius: 10
    },
    inputAddress: {
        borderWidth: 1,
        borderColor: "black",
        backgroundColor: "white",
        height: "60%",
        width:"96%",
        borderRadius: 5,
        margin: "2%",
        paddingRight: "3%",
        paddingLeft: "3%",
    },
    inputName: {
        borderWidth: 1,
        borderColor: "black",
        backgroundColor: "white",
        height: "60%",
        width:"73%",
        borderRadius: 5,
        margin: "2%",
        paddingRight: "3%",
        paddingLeft: "3%",
    },
    searchButton: {
        borderColor: "red",
        borderWidth: 3,
        borderRadius: 10,
        padding: 20
    },
    searchButtonText: {
        color: "red",
        fontWeight: "bold",
        fontSize: 20,
    },
    buttonContainer: {
        flex: 1,
        justifyContent: "flex-end",
        alignItems: "center",
        paddingBottom: "15%"

    }
});
